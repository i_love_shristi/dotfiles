CC = g++ -std=c++17
WARNINGS = -Wall -Wfatal-errors
DEBUG = -g
OPTIMIZE = -O1
MACROS = -Daishwarya_tandon_is_the_best
test: test.cpp
	$(CC) $(WARNINGS) $(MACROS) $(DEBUG) $(OPTIMIZE) test.cpp -o test
