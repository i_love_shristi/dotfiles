#
# ~/.zshrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return


# Varible exports
export EDITOR=nvim
alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

# Import colorscheme from 'wal' asynchronously
# &   # Run the process in the background.
# ( ) # Hide shell job control messages.
(cat ~/.cache/wal/sequences &)

# Alternative (blocks terminal for 0-3ms)
cat ~/.cache/wal/sequences

# To add support for TTYs this line can be optionally added.
source ~/.cache/wal/colors-tty.sh
neofetch --w3m ~/Pictures/wall/vaporwave4.jpg
alias vim=nvim
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
alias bl=~/scripts/bluetooth.sh

#Function for fzf
function doc {
  find ~/Documents | fzf
}

#Function for cd
function hom {
  find ~ \! \( \( -regex '.*/\..*' -o -path ~/ImportantExtract -o -regex '.*/dot.*' -o -path ~/R -o -path ~/pure -o -path ~/NerdFont \) -prune \) -print | fzf
}

#(f)uzzy (f)iles
function ff {
  cd "$(find . \! \( \( -regex '.*/\..*' \) -prune \) -print | fzf)"
}

#(c)urrent (di)rectory
function cdi {
  cd "$(find . \! \( \( -regex '.*/\..*' \) -prune \) -type d -print | fzf)"
}

#(f)ind (d)irectory
function fd {
  cd "$(hom)"
}

#(d)ocuments (d)irectory
function dd {
  cd "$(find ~/Documents -type d | fzf)"
}

#cd into files' directory
function cdf() {
  local file
  local dir
  file=$(fzf +m -q "$1") && dir=$(dirname "$file") && cd "$dir"
}

function hv {
  hom | xargs -e nvim
}

# Set fzf installation directory path
#export FZF_BASE=/path/to/fzf/install/dir

# Uncomment the following line to disable fuzzy completion
# export DISABLE_FZF_AUTO_COMPLETION="true"

# Uncomment the following line to disable key bindings (CTRL-T, CTRL-R, ALT-C)
# export DISABLE_FZF_KEY_BINDINGS="true"

#plugins=(
  #fzf
#)

if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
  source /etc/profile.d/vte.sh
fi

source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source ~/antigen.zsh


source ~/.zplug/init.zsh
zplug "mafredri/zsh-async", from:"github", use:"async.zsh"
# Install plugins if there are plugins that have not been installed
if ! zplug check --verbose; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    fi
fi

# Then, source plugins and add commands to $PATH
zplug load

# Load the oh-my-zsh's library.
antigen use oh-my-zsh

# Bundles from the default repo (robbyrussell's oh-my-zsh).
antigen bundle git
antigen bundle heroku
antigen bundle pip
antigen bundle lein
antigen bundle command-not-found

# Syntax highlighting bundle.
antigen bundle zsh-users/zsh-completions
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-autosuggestions

# Load the theme.
#antigen theme nbitmage/clarity.zsh
POWERLEVEL9K_MODE='awesome-fontconfig'
antigen theme sindresorhus/pure
#antigen apply
# optionally define some options
PURE_CMD_MAX_EXEC_TIME=10
# Tell Antigen that you're done.
antigen apply

PATH="/home/i_love_shristi/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="/home/i_love_shristi/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/home/i_love_shristi/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/home/i_love_shristi/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/home/i_love_shristi/perl5"; export PERL_MM_OPT;
