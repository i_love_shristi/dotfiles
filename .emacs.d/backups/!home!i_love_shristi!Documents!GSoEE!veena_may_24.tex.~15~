% Created 2019-05-24 Fri 09:52
% Intended LaTeX compiler: pdflatex
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\author{Shristi Shreya Singh}
\date{\today}
\title{Work Done}
\hypersetup{
 pdfauthor={Shristi Shreya Singh},
 pdftitle={Work Done},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.2 (Org mode 9.1.9)}, 
 pdflang={English}}
\begin{document}

\maketitle

\section{Work Done}
\label{sec:orgfe7044c}
\subsection{Problem with earlier approach}
\label{sec:org11f4541}
In the earlier work, I had to mark the regions around the polygons manually and individually pixelate a polygon around the polygons that I have drawn. The problem with this approach was that the marking of polygons around the points given to me in various file (like \texttt{KML}, \texttt{shapefiles}) were plenty. Hence, individually tagging all those points which were around 60 for P. Juliflora and around 80 for L. Camara was error prone, time taking and laborious.

Hence, I had to explore an automatic approach using \emph{unsupervised classification}, which automatically tags a region around a given ground truth point.

\subsection{New Methodology}
\label{sec:orgde8dac0}
So, the basic idea was to take a region and then run a clustering algorithm to group its pixels into clusters of P. Juliflora.
Let us consider the original image of a region given to us in the datapoint.
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./ClusterProsopis/original.png}
\caption{Satellite Image of the field}
\end{figure}
The methodology can be stated as follows:
\begin{itemize}
\item Consider the point given in the \texttt{KML}, \texttt{shapefiles}

I took all the points in a \texttt{featureCollection} as imported from the \texttt{KML}, \texttt{shapefiles}. 
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./ClusterProsopis/redMarkLocations.png}
\caption{The red points are the ground truth points collected from the fields.}
\end{figure}
\item Mark a circular region around it of radius \(500m\)

For all the points in the \texttt{featureCollection}, I mapped the points to create circular regions around them using \texttt{this.FeatureCollection.map()}
\begin{center}
\includegraphics[width=.9\linewidth]{./ClusterProsopis/markedArea.png}
\end{center}
\item Let us call this circular region as \texttt{geometry\_point}
\item Run the clustering algorithm on \texttt{geometry\_point} region.
\end{itemize}
\end{document}
