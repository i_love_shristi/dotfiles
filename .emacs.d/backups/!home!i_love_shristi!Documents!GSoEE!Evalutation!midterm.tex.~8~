% Created 2019-07-02 Tue 04:30
% Intended LaTeX compiler: pdflatex
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\author{Shristi Shreya Singh}
\date{\today}
\title{Work Done}
\hypersetup{
 pdfauthor={Shristi Shreya Singh},
 pdftitle={Work Done},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.2 (Org mode 9.1.9)}, 
 pdflang={English}}
\begin{document}

\maketitle

\section{Initial approach}
\label{sec:org2d1e17a}
We were provided with ground truth points for the Prosopis Juliflora and Lantana Camara. The ground truth points provided were of the following regions:

\begin{itemize}
\item \textbf{P. Juliflora} - Tamil Nadu, Gujrat, Western Ghats
\item \textbf{L. Camara} - Uttrakhand
\end{itemize}

We first tried to experiment with the latitude, longitude of the given ground truth points for both P. Juliflora and L. Camara. using the \texttt{inspector} in the dataset. We tested our hypothesis that the points are seperable through their band vectors, and manually started inspecting points and the surrounding vegetation. Upon manually inspecting, we had the following observations:

\begin{enumerate}
\item P. Juliflora is distinguishable with our naked eyes with high resolution imagery and Sentinel-2 imagery.
\item P. Juliflora has a distinguishable band vectors from the surrounding vegetatation.
\end{enumerate}

\subsection{{\bfseries\sffamily TODO} Update Prosopis' locations}
\label{sec:orgf2dbffb}
\subsubsection{{\bfseries\sffamily TODO} Add Images of their locations also}
\label{sec:org82d4174}
\subsection{{\bfseries\sffamily TODO} Update Lantana's locations}
\label{sec:org3dbf80c}
\subsection{{\bfseries\sffamily TODO} Add Photos P. Juliflora}
\label{sec:orgbd15f29}
\subsection{Spectral Analysis}
\label{sec:org94b9d7e}
Hence we first started our pursuit to classify P. Juliflora. We started to mark the polygons of the various classes for our classification. We found this manual process quite exhaustive. It was inaccurate and we tended to miss various hidden polygons. We looked for way to automate this and we used unsupervised classification to automatically perform grouping/clustering of the Prosopis Class.

So, the basic idea was to take a region and then run a clustering algorithm to group its pixels into clusters of P. Juliflora.
Let us consider the original image of a region given to us in the datapoint.
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./../ClusterProsopis/original.png}
\caption{Satellite Image of the field}
\end{figure}
The methodology can be stated as follows:
\begin{itemize}
\item Consider the point given in the \texttt{KML}, \texttt{shapefiles}

I took all the points in a \texttt{featureCollection} as imported from the \texttt{KML}, \texttt{shapefiles}. 
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./../ClusterProsopis/redMarkLocations.png}
\caption{The red points are the ground truth points collected from the fields.}
\end{figure}
\item Mark a circular region around it of radius \(500m\)

For all the points in the \texttt{featureCollection}, I mapped the points to create circular regions around them using \texttt{this.FeatureCollection.map()}
\begin{center}
\includegraphics[width=.9\linewidth]{./../ClusterProsopis/markedArea.png}
\end{center}
\item Let us call this circular region as \texttt{geometry\_point}
\item Run the clustering algorithm on \texttt{geometry\_point} region.

After running the \texttt{wekaKmeans} algorithm given in the Google Earth Engine library, I was able to successfully identify the regions around P. Juliflora. Since the number of clusters is a hyperparameter, I chose it to value of 4 after several hit-trials and seeing that going above 7 clusters takes too much of computation time and doesn't improves the classification by much. Hence, 4 was the best possible choice. You can see the clustered images below.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./../ClusterProsopis/kmeanclassified.png}
\caption{The region marked with pink, is the area covered P. Juliflora. We can observe that P. Juliflora can succesfully distinguished on the basis of it's spectral signatures.}
\end{figure}
\end{itemize}

\subsubsection{How Clustering was achieved}
\label{sec:orge297061}
\begin{enumerate}
\item I first took the \emph{Sentinel-2} image dataset as they provide high resolution dataset upto \(10m\).
\item I considered 4 bands in the \emph{Sentinel-2} dataset, namely, \texttt{R, G, B, NIR, SWIR-1}. All these bands except \texttt{SWIR-1} had 10m spatial resolution, while \texttt{SWIR-1} had 20m resolution.
\item To the final image with 5 bands, two more bands were added
\begin{itemize}
\item \textbf{NDVI}: The normalized difference between \texttt{NIR} and \texttt{R} band
\[
        \frac{NIR - RED}{NIR + RED}
        \]
\item \textbf{EVI}: Enhanced vegetation index
\[
        2.5 \times \frac{NIR - RED}{NIR + 6RED - 7.5Blue + 1}
        \]
\end{itemize}
\item Hence, the final image on which classification had to be performed had 7 bands, \texttt{Red, Green, Blue, NIR, SWIR-1, NDVI, EVI}
\item \texttt{wekaKmeans} Algorithm was run on this to find the clusters.
\end{enumerate}


For L. Camara, we didn't find such patterns. They were largely hidden under the canopies.

\subsection{Textural Analysis}
\label{sec:org6473cfc}
I analysed the textures of the sentinel-2 images and I have analysed various parameters described below as it was proposed during my proposal. The kernel choosen was a square of \(2 * 2\) px wide, which is approximately \(100m^2\) area.
\subsubsection{Entropy}
\label{sec:org3a4dd71}
Most of the images were completely black and there was nothing distinguishable in the image. Including this just increased the computational time of our input.
The image here refers to the transformed image after we calculated the entropy for each pixel of original dataset.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./../TextureProsopis/original.png}
\caption{Here's our original picture of P. Juliflora}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./../TextureProsopis/glcmcontrast.png}
\caption{Here's the same image shown as grayscaled after doing the entropy transformation. As we see, nothing is clearly distinguisable.}
\end{figure}

\pagebreak

\subsubsection{Variance}
\label{sec:org8c11b10}
Variance was the most disappointing to observe as all the images were black due to no difference being seen in the surrounding pixels.

\subsubsection{Standard Deviation}
\label{sec:org8b78f9e}
No use as sentinel's resolution is quite low and there was no change in the values. What we see here is that only EVI and NDVI bands have slight distinguisable property but not enough to distinguish P. Juliflora from the rest of the vegetation and it's also already colored as black.
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./../TextureProsopis/B2.png}
\caption{Standard Deviation under B2 band}
\end{figure}
\pagebreak
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./../TextureProsopis/B3.png}
\caption{Under B3 band}
\end{figure}
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./../TextureProsopis/B4.png}
\caption{Under B4 band}
\end{figure}
\pagebreak
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./../TextureProsopis/EVI.png}
\caption{Under EVI band}
\end{figure}
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./../TextureProsopis/NDVI.png}
\caption{Under NDVI band}
\end{figure}
\pagebreak
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./../TextureProsopis/NIR.png}
\caption{Under NIR band}
\end{figure}
\subsubsection{Mean}
\label{sec:orga16c637}
EVI and NDVI were useful. Although, both of them look quite the same in terms of texture. The improvment that they provide in terms of Neighbourhood is not significant. Some of the results that came are listed below.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./../TextureProsopis/meanevi.png}
\caption{mean of EVI band}
\end{figure}
\pagebreak
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./../TextureProsopis/meanndvi.png}
\caption{mean of NDVI band}
\end{figure}

\subsubsection{GLCM}
\label{sec:orgc59711b}
I did experiments with GLCM also. Since the GEE doesn't supports floating point arithmetic on GLCM based features, so I had to scale the bands of each image by 1000 and then casted it into int32().
\begin{itemize}
\item There were a total of 18 bands in the glcm texture images.
\item Some of those included constrast, asm, correlation, variance and various others.
\end{itemize}

\begin{enumerate}
\item Classification
\label{sec:orgd16729e}
\begin{itemize}
\item We performed k means classification on the glcm image containing various textural features.
\item We also performed k means clustering on the glcm image combined with the spectral information images.
\item In all the cases, we found that the computation time increased but k mean clustering didn't improve at all.
\item In the first case when k mean was performed on the image containing only the glcm bands, all the pixels of the P. Juliflora area were misclassified as the same.
\item Hence, we have to scrape this method in favor of something better.
\end{itemize}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./../TextureProsopis/grayclip.png}
\caption{Here we have the grayscaled version of the image.}
\end{figure}
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./../TextureProsopis/graycontrast.png}
\caption{Here we have the contrast band extracted from the glcm image.}
\end{figure}
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./../TextureProsopis/glcmkmeans.png}
\caption{Here we have the classification ran on the glcmkmeans}
\end{figure}
\end{enumerate}
\end{document}
