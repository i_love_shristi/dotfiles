% Created 2019-04-29 Mon 06:05
% Intended LaTeX compiler: pdflatex
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\author{ILoveShristi}
\date{\today}
\title{How to store Bitcoins}
\hypersetup{
 pdfauthor={ILoveShristi},
 pdftitle={How to store Bitcoins},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.2 (Org mode 9.1.9)}, 
 pdflang={English}}
\begin{document}

\maketitle
\tableofcontents


\section{Simple local storage}
\label{sec:orge9e411a}
\begin{itemize}
\item Wallets
\item Encoding keys: base 58 and QR codes
\item Vanity addresses
\end{itemize}

\section{Hot and cold storage}
\label{sec:org285ff59}
\textbf{Cold Storage}: Some place offline

\textbf{Hot Storage}: Some place online

\section{Splitting and Sharing keys}
\label{sec:org9884105}
Here’s the idea: we want to divide our secret key into some number \(N\) of pieces. We want to do it in such a way that if we're given any \(K\) of those pieces then we'll be able to reconstruct the original secret, but if we're given fewer than \(K\) pieces then we won't be able to learn anything about the original secret.

For \(N = 2, K = 2\) and secret key \(S\), we can create \(R, R \oplus S\). This will always work whenever \(N = K\). For \(N > K\), we need another method shown in the table

\begin{center}
\begin{tabular}{|c|c|c|c|c|}
\hline
Equation(\(N\)) & Degree & Shape & Random Parameters & \(K\)\\
\hline
\((S + RX) \mod P\) & 1 & Line & \(R\) & 2\\
\((S + R_1X + R_2X^2) \mod P\) & 2 & Parabola & \(R_1, R_2\) & 3\\
\((S + R_1X + R_2X^2 + R_3X^3) \mod P\) & 3 & Cubic & \(R_1, R_2, R_3\) & 4\\
\hline
\end{tabular}
\end{center}

\begin{itemize}
\item Lagrange Interpolation allows us to reconstruct a polynomial of degree \(K - 1\) from any \(K\) points on its curve.
\item We're safe even if adversary learns about \(K - 1\) of our secrets and at the same time we can tolerate the loss of up to \(N - K\) of them.
\end{itemize}

\subsection{Threshold Cryptography}
\label{sec:org46861f2}
If we take a key and split this way and we then want to go back and use the key to sign something, we still need to bring the shares together and recalculate the inital secret in order to be able to sign with that key. 

\textbf{Threshold Signature}: Calculate the signatures in decentralized fashion without reconstructing private key on single device. Two factor authentication with your phone and desktop.


\subsection{Multi-signatures}
\label{sec:org107c81a}
Multisignature (multisig) refers to requiring more than one key to authorize a Bitcoin transaction. It is generally used to divide up responsibility for possession of bitcoins. \\


Threshold signatures are a cryptographic technique to take a single key, split it into shares, store them separately, and sign transactions without reconstructing the key. Multi-signatures are a feature of Bitcoin script by which you can specify that control of an address is split between multiple independent keys. While there are some differences between them, they both increase 
To learn more about them follow this \href{https://randomoracle.wordpress.com/2018/07/22/threshold-ecdsa-key-sharding-and-multi-signature-a-comparison/}{Comparision}
\end{document}
